//
//  HeaderProfileView.swift
//  bungkhus-ios-challenge-ui-profile-marchendise
//
//  Created by Bungkhus on 1/9/17.
//  Copyright © 2017 Bungkhus. All rights reserved.
//

import UIKit

class HeaderProfileView: UIView {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelNoMember: UILabel!
    @IBOutlet weak var labelPts: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let user = User.set(user: "Rudi Bian Putrawijaya", email: "rudi.bian@mail.com", noMember: "BU-345NM", pts: 590, photo: "img_profile")
        
        labelName.text = user.name
        labelEmail.text = user.email
        labelNoMember.text = user.noMember
        labelPts.text = String(user.pts)
        if let photo = user.photo {
            imageUser.image = UIImage.init(named:photo)
            print("photo user is exist")
        }
    }

}
