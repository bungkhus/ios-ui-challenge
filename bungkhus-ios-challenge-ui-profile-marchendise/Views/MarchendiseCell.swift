//
//  MarchendiseCell.swift
//  bungkhus-ios-challenge-ui-profile-marchendise
//
//  Created by Bungkhus on 1/9/17.
//  Copyright © 2017 Bungkhus. All rights reserved.
//

import UIKit

class MarchendiseCell: UITableViewCell {

    @IBOutlet weak var imageMarchendise: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelPts: UILabel!
    @IBOutlet weak var buttonReedom: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    var marchandise: Marchendise? {
        didSet {
            guard let marchandise = marchandise else {
                labelTitle.text = ""
                labelPts.text = ""
                return
            }
            if let title = marchandise.title {
                labelTitle.text = title
            } else {
                labelTitle.text = "-"
            }
            labelPts.text = String(marchandise.pts) + " pts"
            if let image = marchandise.image {
                imageMarchendise.image = UIImage.init(named: image)
            }
        }
    }
    
    func setupView() {
        buttonReedom.layer.cornerRadius = buttonReedom.bounds.height / 2
        buttonReedom.clipsToBounds = true
    }

}
