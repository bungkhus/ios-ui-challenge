//
//  UIColorExtension.swift
//  bungkhus-ios-challenge-ui-profile-marchendise
//
//  Created by Bungkhus on 1/9/17.
//  Copyright © 2017 Bungkhus. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue = CGFloat((hex & 0xFF)) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    static func primary() -> UIColor {
        return UIColor(hex: 0xd91a31) //Crimson
    }
    
    static func accent() -> UIColor {
        return UIColor(hex: 0xfcb61e) //Dark tangerine
    }
    
    static func mantis() -> UIColor {
        return UIColor(hex: 0xfcb61e)
    }
    
    static func mediumJungleGreen() -> UIColor {
        return UIColor(hex: 0x2a2a2a)
    }
    
    static func white(whitAlpha: CGFloat) -> UIColor {
        return UIColor(hex: 0xFFFFFF, alpha: whitAlpha)
    }
    
    static func black(whitAlpha: CGFloat) -> UIColor {
        return UIColor(hex: 0x000000, alpha: whitAlpha)
    }

}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
