//
//  MyJournalViewController.swift
//  bungkhus-ios-challenge-ui-profile-marchendise
//
//  Created by Bungkhus on 1/9/17.
//  Copyright © 2017 Bungkhus. All rights reserved.
//

import UIKit

class MyJournalViewController: UIViewController {

    @IBOutlet weak var labelUhOh: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let uhOh = "Uh Oh!\n"
        let noData = "It seems no data here.\nSlide to left, please :)"
        let myAttribute = [ NSFontAttributeName: UIFont(name: "Chalkduster", size: 15.0)! ]
        let myString = NSMutableAttributedString(string: uhOh, attributes: myAttribute )
        let attrString = NSAttributedString(string: noData)
        myString.append(attrString)
        let myRange = NSRange(location: uhOh.characters.count, length: noData.characters.count-2)
        myString.addAttributes([ NSFontAttributeName: UIFont(name: "Avenir", size: 15.0)!, NSForegroundColorAttributeName: UIColor.white(whitAlpha: 0.7) ], range: myRange)
        
        labelUhOh.attributedText = myString
    }

}
