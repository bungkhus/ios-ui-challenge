//
//  MarchendiseViewController.swift
//  bungkhus-ios-challenge-ui-profile-marchendise
//
//  Created by Bungkhus on 1/9/17.
//  Copyright © 2017 Bungkhus. All rights reserved.
//

import UIKit

class MarchendiseViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var marchendises = [Marchendise]()
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupData()
    }
    
    // MARK: - Methods
    
    func setupViews() {
        tableView.register(UINib(nibName: "MarchendiseTableCell", bundle: nil), forCellReuseIdentifier: "MarchendiseCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setupData() {
        let mcd0 = Marchendise.set(marchendise: "Tridatu Stainless Bottle Grey", pts: 350, image: "img_list_a")
        let mcd1 = Marchendise.set(marchendise: "Lorem Ipsum Dolor", pts: 350, image: "img_list_b")
        let mcd2 = Marchendise.set(marchendise: "The Tridatu Clock Red", pts: 100, image: "img_list_c")
        let mcd3 = Marchendise.set(marchendise: "Lorem Ipsum Dolor", pts: 100, image: "img_list_b")
        let mcd4 = Marchendise.set(marchendise: "Tridatu Stainless Bottle Grey", pts: 350, image:
            "img_list_a")
        
        marchendises.append(mcd0)
        marchendises.append(mcd1)
        marchendises.append(mcd2)
        marchendises.append(mcd3)
        marchendises.append(mcd4)
    }

}

// MARK: - TableView

extension MarchendiseViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return marchendises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarchendiseCell", for: indexPath) as! MarchendiseCell
        cell.marchandise = marchendises[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 119
    }
    
}
