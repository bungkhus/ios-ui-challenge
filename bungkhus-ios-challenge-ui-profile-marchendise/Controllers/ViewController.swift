//
//  ViewController.swift
//  bungkhus-ios-challenge-ui-profile-marchendise
//
//  Created by Bungkhus on 1/9/17.
//  Copyright © 2017 Bungkhus. All rights reserved.
//

import UIKit
import MXSegmentedPager

class ViewController: MXSegmentedPagerController {

    private var title0 = "My Journal"
    private var title1 = "Marchendise"
    private var alreadyReload = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.black(whitAlpha: 0.2)
        
        // Parallax Header
        let xib = Bundle.main.loadNibNamed("HeaderProfileView", owner: nil, options: nil)?[0] as! HeaderProfileView
        self.segmentedPager.parallaxHeader.view = xib
        self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.fill
        self.segmentedPager.parallaxHeader.height = UIScreen.main.bounds.width / 1.5 //189
        self.segmentedPager.parallaxHeader.minimumHeight = 20
        
        // Segmented Control (tab) customization
        self.segmentedPager.segmentedControl.backgroundColor = UIColor.primary()
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white(whitAlpha: 0.7), NSFontAttributeName: UIFont(name: "Avenir", size: 15)!]
        self.segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName : UIColor.accent(), NSFontAttributeName: UIFont(name: "Avenir", size: 15)!]
        self.segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.accent()
        self.segmentedPager.segmentedControl.selectionIndicatorHeight = 1.0
        self.segmentedPager.segmentedControl.selectionIndicatorEdgeInsets = UIEdgeInsetsMake(0, 34, -10, 64)
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyle.textWidthStripe
        self.segmentedPager.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        self.segmentedPager.segmentedControl.setSelectedSegmentIndex(1, animated: false)
//        self.segmentedPager.reloadData()
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return [title0, title1][index];
    }

}

