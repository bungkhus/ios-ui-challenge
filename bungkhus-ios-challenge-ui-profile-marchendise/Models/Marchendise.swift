//
//  Marchendise.swift
//  bungkhus-ios-challenge-ui-profile-marchendise
//
//  Created by Bungkhus on 1/9/17.
//  Copyright © 2017 Bungkhus. All rights reserved.
//

import Foundation

class Marchendise: NSObject {
    
    var title: String?
    var pts: Int = 0
    var image: String?
    
    static func set(marchendise title: String, pts: Int, image: String) -> Marchendise {
        let marchendise = Marchendise()
        marchendise.title = title
        marchendise.pts = pts
        marchendise.image = image
        
        return marchendise
    }
}
