//
//  User.swift
//  bungkhus-ios-challenge-ui-profile-marchendise
//
//  Created by Bungkhus on 1/9/17.
//  Copyright © 2017 Bungkhus. All rights reserved.
//

import Foundation

class User: NSObject {
    
    var name: String?
    var pts: Int = 0
    var photo: String?
    var email: String?
    var noMember: String?
    
    static func set(user name: String, email: String, noMember: String, pts: Int, photo: String) -> User {
        let user = User()
        user.name = name
        user.email = email
        user.noMember = noMember
        user.pts = pts
        user.photo = photo
        
        return user
    }
}
